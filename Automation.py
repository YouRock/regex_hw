from Node import nodes, make_node, add_to_next, set_terminal
from copy import copy

class AutomationBuilder:
    def __init__(self):
        self._stack = []

    def build_automation(self, rexp: str) -> ([], []):
        for char in rexp:
            if char == '*':
                self._star()
            elif char == '+':
                self._or()
            elif char == '.':
                self._concatinate()
            else:
                self._add_letter(char)

        result = self._stack.pop()
        for node in result._term_nodes:
            nodes[node].is_terminal = True

        return result

    def _star(self):
        automation = self._stack.pop()

        for term_node in automation._term_nodes:
            for start_node in automation._start_nodes:
                add_to_next(term_node, ('', start_node))

        for node in automation._start_nodes:
            set_terminal(node, True)

        automation._term_nodes += automation._start_nodes
        self._stack.append(automation)

    def _or(self):
        auto2 = self._stack.pop()
        auto1 = self._stack.pop()

        root = make_node()

        for start_node in auto1._start_nodes:
            add_to_next(root, ('', start_node))

        for start_node in auto2._start_nodes:
            add_to_next(root, ('', start_node))

        self._stack.append(Automation([root], auto1._term_nodes + auto2._term_nodes))

    def _add_letter(self, char: str):
        finish_node = make_node()
        start_node = make_node([(char, finish_node)])
        self._stack.append(Automation([start_node],[finish_node]))

    def _concatinate(self):
        auto2 = self._stack.pop()
        auto1 = self._stack.pop()

        for term_node in auto1._term_nodes:
            for start_node in auto2._start_nodes:

                add_to_next(term_node, ('', copy(start_node)))

        auto2._start_nodes = auto1._start_nodes

        self._stack.append(auto2)

class Automation:
    def __init__(self, start_nodes=list(), term_nodes=list()):
        self._stack = []
        self._start_nodes, self._term_nodes = start_nodes, term_nodes

    def __contains__(self, word):
        for start_node in self._start_nodes:
            if self._can_rich_term_node(start_node, word):
                return True

        return False

    def _can_rich_term_node(self, node_id, word):
        if word == '':
            return True if nodes[node_id].is_terminal else False
        else:
            for char, next_node_id in nodes[node_id].next:
                if char == word[0] or char == '':
                    if self._can_rich_term_node(next_node_id, word[1:] if char != '' else word):
                        return True