from Node import Node, nodes
from unittest import TestCase
from Automation import AutomationBuilder
from collections import namedtuple
from Parser import Parser

stack = []

Test = namedtuple('Test', ['regex', 'word', 'result'])

def _add_letter(char: str):
    finish_node = Node()
    start_node = Node([(char, finish_node)])
    stack.append(([start_node], [finish_node]))

    finish_node.is_terminal = True
    print(stack[0][0][0].next[0].node.is_terminal)

class TestOperations(TestCase):
    def test_concat(self):
        self._abuilder = AutomationBuilder()
        automation = self._abuilder.build_automation('ab.')

        tests = [('ab', True), ('a', False), ('', False)]
        self._test_automation(automation, tests)

    def test_star(self):
        self._abuilder = AutomationBuilder()
        automation = self._abuilder.build_automation('a*')

        tests = [('aaaa', True), ('a', True), ('', True), ('b', False)]
        self._test_automation(automation, tests)

    def test_plus(self):
        self._abuilder = AutomationBuilder()
        automation = self._abuilder.build_automation('ab+')

        tests = [('aaaa', True), ('a', True), ('', True), ('b', False)]

    def test_chars(self):
        self._abuilder = AutomationBuilder()
        automation = self._abuilder.build_automation('a')

        tests = [('aa', False), ('a', True), ('', False), ('b', False)]

    def _test_automation(self, automation, tests):
        for test in tests:
            print(test)
            self.assertEqual(test[0] in automation, test[1])

    def test_default_tests(self):
        tests = [Test('ab+c.aba.*.bac.+.+*', 'abacb', 4)]

        parser = Parser()
        for test in tests:
            self.assertEqual(parser.solve(test.word, test.regex), test.result)


if __name__ == '__main__':
    import unittest
    unittest.main()
