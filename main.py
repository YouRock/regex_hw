from Automation import AutomationBuilder

def main():
    rexp = input()
    word = input()

    abuilder = AutomationBuilder()
    automation = abuilder.build_automation(rexp)

    for i in range(len(word)):
        if word in automation:
            print(len(word))
            return
        else:
            word = word[:-1]

    print(0)

if __name__ == '__main__':
    main()




