from Automation import AutomationBuilder
from Node import nodes

class Parser:
    def solve(self, word, rexp):
        abuilder = AutomationBuilder()
        automation = abuilder.build_automation(rexp)

        for i in range(len(word)):
            if word in automation:
                return len(word)

            word = word[:-1]

        return 0

