from collections import namedtuple
from copy import copy
#Arrow = namedtuple('Arrow', ['char', 'node'])
nodes = []

def make_node(next=list(), is_terminal=False):
    nodes.append(_Node(copy(next), copy(is_terminal)))
    return len(nodes)-1

def add_to_next(id, val):
    nodes[id].next.append(copy(val))

def set_terminal(id, val):
    nodes[id].is_terminal = val

class Node:


    def __init__(self, next=list(), is_terminal=False):
        self._id = len(nodes)

        node = _Node(next, is_terminal)
        nodes.append(node)

    @property
    def is_terminal(self):
        return nodes[self._id].is_terminal

    @is_terminal.setter
    def is_terminal(self, val):
        nodes[self._id].is_terminal = val

    # @property
    # def next(self):
    #     return nodes[self._id].next
    #
    # @next.setter
    # def next(self, val):
    #     nodes[self._id].next = val




class _Node:
    def __init__(self, next=list(), is_terminal=False):
        self.is_terminal = copy(is_terminal)
        self.next = copy(next)
